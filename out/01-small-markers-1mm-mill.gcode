(Exported by FreeCAD)
(Post Processor: grbl_post)
(Output Time:2021-04-01 18:43:12.218208)
(Begin preamble)
G17 G90
G21
(Begin operation: Fixture)
(Path: Fixture)
G54
(Finish operation: Fixture)
(Begin operation: endmill-1mm-7mm004)
(Path: endmill-1mm-7mm004)
(endmill-1mm-7mm004)
(Begin toolchange)
( M6 T1.0 )
M3 S8000.0
(Finish operation: endmill-1mm-7mm004)
(Begin operation: Drilling)
(Path: Drilling)
(Drilling)
(Begin Drilling)
G0 Z5.000
G90
G99
G83 X-0.000 Y-84.000 Z-5.500 F6.000 Q0.500 R1.000
G80
G0 Z3.000
G83 X54.000 Y-84.000 Z-5.500 F6.000 Q0.500 R1.000
G80
G0 Z3.000
G0 Z5.000
(Finish operation: Drilling)
(Begin postamble)
M5
G17 G90
M2
